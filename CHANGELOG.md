# Changelog

## v1.0.3

* process battle results of custom game modes

## v1.0.2

* fix list of fields to be cleared before serialization

## v1.0.1

* fix bug with bots in battle

## v1.0.0

* initial release