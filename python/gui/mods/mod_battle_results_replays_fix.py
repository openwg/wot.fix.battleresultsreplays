# SPDX-License-Identifier: MIT
# Copyright (c) 2023-2025 Andrii Andrushchyshyn

__version__ = '1.0.3'

import copy
import functools
import json
import os
import struct
import threading
import time

import BigWorld

from Account import PlayerAccount
from AccountCommands import RES_NON_PLAYER, RES_COOLDOWN, RES_STREAM, RES_CACHE, RES_FAILURE
from BattleReplay import REPLAY_FILE_EXTENSION, AUTO_RECORD_TEMP_FILENAME, FIXED_REPLAY_FILENAME
from BattleReplay import _JSON_Encode
from chat_shared import SYS_MESSAGE_TYPE
from gui.shared import g_eventBus
from gui.shared.events import GUICommonEvent
from gui.shared.personality import ServicesLocator
from messenger.proto.events import g_messengerEvents
from PlayerEvents import g_playerEvents

# replays older than 1 hour will be skiped
RECENT_REPLAY_INTERVAL = 1 * 60 * 60
# default folder where stored replays 
REPLAYS_FOLDER = 'replays'
# number of bytes to pack technical data
DEFAULT_PACK_SIZE = 4
# one battle cannot be requested more than 50 times
MAX_PARSE_ATTEMPS = 50
# battle cannot be requested more often than 100 milisecounds
DEFAULT_PARSER_TIMEOUT = 0.1

SUPPROTED_RESULT_MESSAGES = [
	# default battle results
	'battleResults',

	# story_mode_common/battle_mode.py
	# StoryModeBattleMode._SM_TYPE_BATTLE_RESULT
	'storyModeBattleResults',

	# cosmic_event_common/cosmic_constants.py
	# CosmicEventBattleMode._SM_TYPE_BATTLE_RESULT
	'cosmicEventBattleResults',

	# VersusAIPersonality.py
	# ClientVersusAIBattleMode._SM_TYPE_BATTLE_RESULT
	'versusAIBattleResults'
]

def is_supported_battle_results(messageTypeID):
	try:
		return str(SYS_MESSAGE_TYPE[messageTypeID]) in SUPPROTED_RESULT_MESSAGES
	except:
		return False

class ReplayWrapper(object):

	def __init__(self, file_path):
		self._file_path = file_path
		self._header = None
		self._blocks = []
		self._common = None
		self.read_blocks()

	def read_blocks(self):
		offset = 0
		with open(self._file_path, 'rb') as fh:
			# first store magic
			fh.seek(offset)
			self._header = struct.unpack('I', fh.read(DEFAULT_PACK_SIZE))[0]
			offset += DEFAULT_PACK_SIZE
			# get current replay blocks count
			fh.seek(offset)
			blocks_count = struct.unpack('I', fh.read(DEFAULT_PACK_SIZE))[0]
			offset += DEFAULT_PACK_SIZE
			self._blocks = []
			# get blocks
			for _ in range(blocks_count):
				fh.seek(offset)
				size = struct.unpack('I', fh.read(DEFAULT_PACK_SIZE))[0]
				offset += DEFAULT_PACK_SIZE
				fh.seek(offset)
				self._blocks.append(fh.read(size))
				offset += size

		self._raw_start = offset
		if self._blocks:
			self._common = json.loads(self._blocks[0])

	def store_blocks(self):
		# get replay raw data
		with open(self._file_path, 'rb') as fh:
			fh.seek(self._raw_start)
			raw = fh.read()
		# store in replay blocks and raw data
		with open(self._file_path, 'wb') as fh:
			fh.write(struct.pack('II', self._header, len(self._blocks)))
			for block in self._blocks:
				fh.write(struct.pack('I', len(block)))
				fh.write(block)
			fh.write(raw)
		# reload blocks after store
		self.read_blocks()

	def has_personal(self):
		return len(self._blocks) > 1

	def add_personal(self, personal):
		vehicles = self._common.get('vehicles')
		statistics = {x: {'frags': y[0]['kills']} for x, y in personal.get('vehicles', {}).items()}
		personal = fix_personal_data(personal)
		self._blocks.append(json.dumps([personal, vehicles, statistics]))
		self.store_blocks()

	def get_vehicles_hash(self):
		if self._common:
			return get_hash_by_replay(self._common)

def fix_personal_data(results):
	modifiedResults = copy.deepcopy(results)
	allPlayersVehicles = modifiedResults.get('vehicles', None)
	if allPlayersVehicles is not None:
		for playerVehicles in allPlayersVehicles.itervalues():
			for vehicle in playerVehicles:
				if vehicle is not None:
					vehicle['damageEventList'] = None
	personals = modifiedResults.get('personal', None)
	if personals is not None:
		for personal in personals.itervalues():
			for field in ('damageEventList', 'xpReplay', 'creditsReplay', 'tmenXPReplay', 'goldReplay', 
			 'crystalReplay', 'freeXPReplay', 'avatarDamageEventList', 'eventCoinReplay', 'bpcoinReplay', 
			 'equipCoinReplay', 'battlePassPointsReplay', 'flXPReplay', 'paragonCoinsReplay', ):
				if field in personal:
					personal[field] = None
			for currency in personal.get('currencies', {}).itervalues():
				if 'replay' in currency:
					currency['replay'] = None
			extMeta = personal.get('ext', {}).get('epicMetaGame')
			if extMeta is not None:
				for field in ('flXPReplay',):
					if field in extMeta:
						extMeta[field] = None
	common = modifiedResults.get('common', None)
	if common is not None:
		if 'accountCompDescr' in common:
			common['accountCompDescr'] = None
	return _JSON_Encode(modifiedResults)

# get hash from local battle results
def get_hash_by_replay(data):
	if not data:
		return
	result = 0
	for vehData in data.get('vehicles', {}).values():
		sessionID = vehData.get('avatarSessionID', '')
		if sessionID:
			result += int(sessionID)
	return result

# get hash from server battle results
def get_hash_by_results(data):
	if not data:
		return
	result = 0
	for sessionID, vehData in data.get('vehicles', {}).items():
		if vehData[0]['accountDBID']:
			result += int(sessionID)
	return result

def get_fresh_broken_replays():
	minimal_timestamp = time.time() - RECENT_REPLAY_INTERVAL
	for file_name in os.listdir(REPLAYS_FOLDER):
		file_path = '%s/%s' % (REPLAYS_FOLDER, file_name)
		if not file_name.endswith(REPLAY_FILE_EXTENSION):
			continue
		if AUTO_RECORD_TEMP_FILENAME in file_name:
			continue
		if FIXED_REPLAY_FILENAME in file_name:
			continue
		if os.path.getmtime(file_path) < minimal_timestamp:
			continue
		replay = ReplayWrapper(file_path)
		if not replay.has_personal():
			yield replay

def on_battle_results(_, results):
	# get battle result hash
	battle_vehicles_hash = get_hash_by_results(results)
	# try fix broken replays with same hash
	for replay in get_fresh_broken_replays():
		if battle_vehicles_hash != replay.get_vehicles_hash():
			continue
		replay.add_personal(results)

class BattleResultProcessor:

	def __init__(self):
		self.__queue = []
		self.__terminated = False
		self.__available = threading.Event()
		self.__available.clear()
		self.__busy = threading.Lock()
		self.__handlers = set([])

	def subscribe(self, handler):
		if handler not in self.__handlers:
			self.__handlers.add(handler)

	def init(self):
		# subscribe for events
		g_eventBus.addListener(GUICommonEvent.LOBBY_VIEW_LOADED, self.resume)
		g_playerEvents.onAccountBecomeNonPlayer += self.pause
		g_messengerEvents.serviceChannel.onChatMessageReceived += self.on_message
		# start main loop thread
		thread = threading.Thread(target=self.__loop)
		thread.setDaemon(True)
		thread.start()

	def fini(self):
		self.__terminated = True
		# release block
		self.__available.set()
		# unsubscribe
		g_eventBus.removeListener(GUICommonEvent.LOBBY_VIEW_LOADED, self.resume)
		g_playerEvents.onAccountBecomeNonPlayer -= self.pause
		g_messengerEvents.serviceChannel.onChatMessageReceived -= self.on_message

	def resume(self, _):
		# release block
		callback = functools.partial(self.__available.set)
		BigWorld.callback(DEFAULT_PARSER_TIMEOUT, callback)

	def pause(self):
		# set block
		self.__available.clear()
		# release busy
		if self.__busy.locked():
			self.__busy.release()

	def on_message(self, _, message):
		# skip battle if message is not battleResults
		if not is_supported_battle_results(message.type):
			return
		# put battle to queue
		arenaUniqueID = message.data.get('arenaUniqueID', 0)
		self.__queue.append((arenaUniqueID, 0))

	def __loop(self):
		while not self.__terminated:
			# wait for release block
			self.__available.wait()
			# break loop logic if we are done
			if self.__terminated:
				break
			# skip loop if queue list empty
			if not self.__queue:
				time.sleep(DEFAULT_PARSER_TIMEOUT)
				continue
			# acquire a blocking lock
			self.__busy.acquire()
			# get data from queue
			player = BigWorld.player()
			arenaUniqueID, parseAttemp = self.__queue.pop()
			# in case Account and synces items
			if isinstance(player, PlayerAccount) and ServicesLocator.itemsCache.isSynced():
				callback = functools.partial(self.__handle_response, arenaUniqueID, parseAttemp)
				player.battleResultsCache.get(arenaUniqueID, callback)
			else:
				callback = functools.partial(self.__handle_retry, arenaUniqueID, parseAttemp)
				BigWorld.callback(DEFAULT_PARSER_TIMEOUT, callback)
	
	def __handle_retry(self, arenaUniqueID, parseAttemp):
		# skip this battle on max attempt reached
		# return battle with req counter to queue 
		if parseAttemp < MAX_PARSE_ATTEMPS:
			self.__queue.append((arenaUniqueID, parseAttemp + 1))
		# release lock
		if self.__busy.locked():
			self.__busy.release()

	def __handle_response(self, arenaUniqueID, parseAttemp, responseCode, results=None):
		# handle normal errors
		if responseCode in (RES_NON_PLAYER, RES_COOLDOWN, RES_FAILURE):
			callback = functools.partial(self.__handle_retry, arenaUniqueID, parseAttemp)
			BigWorld.callback(DEFAULT_PARSER_TIMEOUT, callback)
			return
		# handle response
		if responseCode in (RES_STREAM, RES_CACHE):
			for handler in self.__handlers:
				if handler and callable(handler):
					handler(arenaUniqueID, results)
		# release lock
		if self.__busy.locked():
			self.__busy.release()

results_processor = BattleResultProcessor()

def init():
	results_processor.init()
	results_processor.subscribe(on_battle_results)

def fini():
	results_processor.fini()
